package com.kebhana.postit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostitServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostitServiceApplication.class, args);
	}

}
