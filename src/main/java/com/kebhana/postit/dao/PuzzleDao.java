package com.kebhana.postit.dao;



import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.postit.model.PuzzleModel;
import com.kebhana.postit.model.TermsModel;
import com.kebhana.postit.model.ProductModel;
import com.kebhana.postit.model.AccountModel;
import com.kebhana.postit.model.StepSaveModel;
import com.kebhana.postit.model.EventModel;
import com.kebhana.postit.model.LoginModel;

@Mapper
public interface PuzzleDao {

	/**
	 * 사용자 전체 정보 가져오기 
	 * @return
	 * @throws Exception
	 */

	// [GET] DB 정보를 가져온다.
	String selectDBinfo() throws Exception;
	
	// [GET] 퍼즐 정보를 가져온다.
	List<PuzzleModel> selectPuzzleInfo(int puzzleId) throws Exception;	
	

	// [GET] 퍼즐 이미지 경로를 가져온다.
	String selectPuzzlePath(int puzzleId) throws Exception;
	
	// [GET] 약관정보를 가져온다.
	List<TermsModel> selectTermsInfo(int termsId) throws Exception;	
	
	// [POST] 퍼즐 정보를 입력한다.
	//int insertPuzzleInfo(PuzzleModel puzzleModel) throws Exception;

	// [GET] 테마에 따른 상품정보를 가져온다.
	List<ProductModel> selectProductInfo(int productId) throws Exception;	
	

	// [POST] 가입을 완료한다.
	int insertAccount(AccountModel accountModel) throws Exception;

	// [POST] 당첨여부를 설정한다.
	int insertEvent(EventModel eventModel) throws Exception;

	// [GET] 당첨내용을 가져온다.
	List<EventModel> selectEventInfo(String custNo, String acctNo) throws Exception;	
	
	// [GET] 고객번호에 따른 요구불 통장 목록을 가져온다.
	List<AccountModel> selectAccountInfo1(int custNo) throws Exception;

	// [GET] 고객번호에 따른 적금 통장 목록을 가져온다.
	List<AccountModel> selectAccountInfo2(int custNo) throws Exception;




	
	//--------------------------------------------------------
	//▼▼▼▼▼▼▼ 유영미 차자님 작성 ▼▼▼▼▼▼▼
	//--------------------------------------------------------
	// [GET] stepsave 정보를 가져온다.
	List<StepSaveModel> selectStepsaveInfo(String custNo, String prdCd) throws Exception;   ;   
	
	// [PUT] STEP단계를 저장한다 .
	int insertStepSave(StepSaveModel stepSaveModel) throws Exception;   

	// [PUT] STEP단계를 update한다 .
	int updateStepSave(StepSaveModel stepSaveModel) throws Exception;   

	// [PUT] STEP 4단계완료  delete한다 .
	int deleteStepSave(StepSaveModel stepSaveModel) throws Exception;

	// [PUT] 고객(로그인)  저장한다 .
	int insertLoginInfo(LoginModel loginModel) throws Exception; 

	// [GET] 고객(로그인)  가져온다.
	List<LoginModel> selectLoginInfo(String custNo) throws Exception; 

}