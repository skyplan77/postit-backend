package com.kebhana.postit.rest;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Date;
import java.text.SimpleDateFormat;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kebhana.postit.dao.PuzzleDao;
import com.kebhana.postit.model.Hello;
import com.kebhana.postit.model.PuzzleModel;
import com.kebhana.postit.model.ResAccountModel;
import com.kebhana.postit.model.TermsModel;
import com.kebhana.postit.model.ProductModel;
import com.kebhana.postit.model.AccountModel;
import com.kebhana.postit.model.StepSaveModel;
import com.kebhana.postit.model.EventModel;
import com.kebhana.postit.model.LoginModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="Hello Service API")
@RestController
public class PuzzleController {
	private String msgTemplate = "%s님  반갑습니다.";
	private final AtomicLong  vistorConouter = new AtomicLong();
	
	
	@Autowired
	private PuzzleDao puzzleDao;
	
	//--------------------------------------------------------
	// 연결된 DB의 정보를 가져온다.
	//--------------------------------------------------------
	@ApiOperation(value="DB정보 가져오기")
	@RequestMapping(value="/dbinfo", method=RequestMethod.GET)
	public ResponseEntity <String> getDBinfo() {
		
		double dcouponDecision = Math.random();
		int tcouponDecision = (int)(dcouponDecision * 42)+1;
		int icouponDecision = (int)(tcouponDecision/10);
		System.out.println("추출값 : "+tcouponDecision+" / 변환값 : "+icouponDecision);
		
		//쿠폰 지급시 대상항목 선정
		switch(icouponDecision){
		case 0 : 
			System.out.println("상품은 : STARBUCKS Coupon");
			break;
		case 1 : 
			System.out.println("상품은 : COFFEEBEAN Coupon");
			break;
		case 2 : 
			System.out.println("상품은 : TWOSOME PLACE Coupon");
			break;
		case 3 : 
			System.out.println("상품은 : BASKIN ROBBINS Coupon");
			break;
		case 4 : 
			System.out.println("상품은 : ★★★★★GREAAAAAAAAAAT★★★★★ Coupon");
		}
	
		String result = "";
		
		try {
			log.info("Start get DB information");
			result = puzzleDao.selectDBinfo();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("db information :" + result);
		
		return new ResponseEntity<String> (result, HttpStatus.OK);
		
	}


	//--------------------------------------------------------
	//퍼즐 ID에 대한 정보를 가져온다. (퍼즐명, 퍼즐경로)
	//--------------------------------------------------------
	@ApiOperation(value="퍼즐 정보 가져오기")
	@RequestMapping(value="/puzzle/{puzzleId}", method=RequestMethod.GET)
	public ResponseEntity <List<PuzzleModel>> getPuzzleInfo(
			@PathVariable (name="puzzleId", required = true) int puzzleId
		) {
		
		List<PuzzleModel> result = null;
		try {
			log.info("Start get Puzzle Information");
			result = puzzleDao.selectPuzzleInfo(puzzleId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("puzzle information :" + result.size());
		
		return new ResponseEntity<List<PuzzleModel>> (result, HttpStatus.OK);
	}




	//--------------------------------------------------------
	//상품ID에 따른 약관정보를 가져온다.(약관이름, 약관경로)
	//--------------------------------------------------------
	@ApiOperation(value="약관 정보 가져오기")
	@RequestMapping(value="/terms/{productTermsId}", method=RequestMethod.GET)
	public ResponseEntity <List<TermsModel>> getTermsInfo(
			@PathVariable (name="productTermsId", required = true) int productTermsId
		) {
		
		List<TermsModel> result = null;
		try {
			log.info("Start get Terms Information");
			result = puzzleDao.selectTermsInfo(productTermsId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("terms information :" + result.size());
		
		return new ResponseEntity<List<TermsModel>> (result, HttpStatus.OK);
	}




	//--------------------------------------------------------
	//테마에 따른 상품목록을 가져온다.
	//--------------------------------------------------------
	@ApiOperation(value="상품 정보 가져오기")
	@RequestMapping(value="/product/{themeId}", method=RequestMethod.GET)
	public ResponseEntity <List<ProductModel>> getProductInfo(
			@PathVariable (name="themeId", required = true) int themeId
		) {
		
		List<ProductModel> result = null;
		try {
			log.info("Start get Product Information");
			result = puzzleDao.selectProductInfo(themeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("product information :" + result.size());
		
		return new ResponseEntity<List<ProductModel>> (result, HttpStatus.OK);
	}




	//--------------------------------------------------------
	//고객번호에 따른 요구불 통장 목록을 가져온다.
	//--------------------------------------------------------
	@ApiOperation(value="요구불 계좌 정보 가져오기")
	@RequestMapping(value="/account/regular/{custNo}", method=RequestMethod.GET)
	public ResponseEntity <List<AccountModel>> getAccountInfo1(
			@PathVariable (name="custNo", required = true) int custNo
		) {
		
		List<AccountModel> result = null;
		try {
			log.info("Start get Regular Account Information");
			result = puzzleDao.selectAccountInfo1(custNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("Account information :" + result.size());
		
		return new ResponseEntity<List<AccountModel>> (result, HttpStatus.OK);
	}



	//--------------------------------------------------------
	//고객번호에 따른 적금 통장 목록을 가져온다.
	//--------------------------------------------------------
	@ApiOperation(value="적금 계좌 정보 가져오기")
	@RequestMapping(value="/account/installment/{custNo}", method=RequestMethod.GET)
	public ResponseEntity <List<AccountModel>> getAccountInfo2(
			@PathVariable (name="custNo", required = true) int custNo
		) {
		
		List<AccountModel> result = null;
		try {
			log.info("Start get Installment Account Information");
			result = puzzleDao.selectAccountInfo2(custNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("Account information :" + result.size());
		
		return new ResponseEntity<List<AccountModel>> (result, HttpStatus.OK);
	}



	//--------------------------------------------------------
	//선택한 상품을 가입한다.
	//--------------------------------------------------------
	@ApiOperation(value="상품 가입하기")
	@RequestMapping(value="/account", method=RequestMethod.POST)
	public ResponseEntity <ResAccountModel> postAccount(
			@RequestBody AccountModel accountModel
			) throws Exception { 
		
		int resultAccount=-1;
		int resultWin=-1;
		int acctmp1=-1;
		int acctmp2=-1;
		String accountNumber="";
		String twinType="";
		String twinValue="";
		
		ResAccountModel resAccountModel = new ResAccountModel();

		// 현재시간을 가져와 Date형으로 저장한다
		Date date_now = new Date(System.currentTimeMillis());
		
		// 이벤트 설정 모델
		EventModel eventModel = new EventModel();
		
		try {
			log.info("Start open an Account");
			
			
			//계좌번호 생성
			acctmp1 = (int)Math.floor(Math.random() * 1000000)+100000;
			acctmp2 = (int)Math.floor(Math.random() * 100000)+10000;
			if(acctmp1>1000000){
				acctmp1 = acctmp1 - 100000;
			} 
			if(acctmp2>100000){
				acctmp2 = acctmp2 - 10000;
			}
			accountNumber="396-" + acctmp1 + "-" + acctmp2;
			accountModel.setAcctNo(accountNumber);
			accountModel.setPrdType("2");
			//accountModel.setPrdCd("installment savings");
			resultAccount = puzzleDao.insertAccount(accountModel);

			//계좌생성여부가 정상이면
			if (resultAccount == 1 ) {
				
				//당첨 여부 생성
				double dwinDecision = Math.random();
				int iwinDecision = (int)(dwinDecision * 200)+1;

				// 1~20까지는 머니로 지급, 21~200까지는 쿠폰으로 지급
				if (iwinDecision < 21) {
					twinType="A";
					twinValue=Integer.toString(100);
					eventModel.setWinType(twinType);
					eventModel.setWinValue(twinValue);
				}else {
					double dcouponDecision = Math.random();
					int icouponDecision = (int)((dcouponDecision * 42)+1)/10;
					
					//쿠폰 지급시 대상항목 선정
					//wiType : B - 일반쿠폰, C - 스페셜 쿠폰
					switch(icouponDecision){
					case 0 : 
						twinType="B";
						eventModel.setWinType(twinType);
						twinValue="STARBUCKS Coupon";
						eventModel.setWinValue(twinValue);
						break;
					case 1 : 
						twinType="B";
						eventModel.setWinType(twinType);
						twinValue="COFFEEBEAN Coupon";
						eventModel.setWinValue(twinValue);
						break;
					case 2 : 
						twinType="B";
						eventModel.setWinType(twinType);
						twinValue="TWOSOME PLACE Coupon";
						eventModel.setWinValue(twinValue);
						break;
					case 3 : 
						twinType="B";
						eventModel.setWinType(twinType);
						twinValue="BASKIN ROBBINS Coupon";
						eventModel.setWinValue(twinValue);
						break;
					case 4 : 
						twinType="C";
						eventModel.setWinType(twinType);
						twinValue="!!!!!!!!! SPECIAL GIFT Coupon !!!!!!!!!";
						eventModel.setWinValue(twinValue);
					}
				}
				
				// 년월일시분초 14자리 포멧
				SimpleDateFormat fourteen_format = new SimpleDateFormat("yyyy-MM-dd"); 
				
				eventModel.setAcctNo(accountNumber);
				eventModel.setCustNo(accountModel.getCustNo());
				//eventModel.setPrdCd(accountModel.getPrdCd());
				//eventModel.setPrdName(accountModel.getPrdName());
				//eventModel.setPuzzleId("1");
				//eventModel.setThemeId("1");
				eventModel.setWinDate(fourteen_format.format(date_now));
				resultWin = puzzleDao.insertEvent(eventModel);
			}

			if (resultAccount==1 && resultWin==1) {
				resAccountModel.setResultMsg("[OK] Open an Account and Decision Win Success!");
				resAccountModel.setResultCode("1");
				resAccountModel.setAccountNumber(accountNumber);
				resAccountModel.setWinType(twinType);
				resAccountModel.setWinValue(twinValue);
			}
			else {
				resAccountModel.setResultMsg("[ERROR] Open an Account and Decision Win unsuccess!");
				resAccountModel.setResultCode("-1");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("resultAccount :"+ resultAccount + " / resultWin : " + resultWin);
		
		return new ResponseEntity<ResAccountModel> (resAccountModel, HttpStatus.OK);
	}




	//--------------------------------------------------------
	//당첨내역를 조회한다. 당첨정보를 랜덤하게 생성하여 입력한다.
	//winType : A(일정금액 당첨), B(쿠폰당첨)
	//winValue : 금액 또는 쿠폰 정보
	//--------------------------------------------------------
	@ApiOperation(value="당첨 내역(금액 or 쿠폰) 가져오기")
	@RequestMapping(value="/event/{custNo}", method=RequestMethod.GET)
	public ResponseEntity <List<EventModel>> getEventInfo(
			@PathVariable (name="custNo", required = true) String custNo,
			@RequestParam(value = "acctNo", required = false) String acctNo
			) {
		List<EventModel> result = null;

		try {
			log.info("Start get event information");
			result = puzzleDao.selectEventInfo(custNo,acctNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("result :"+ result.size());
		
		return new ResponseEntity<List<EventModel>> (result, HttpStatus.OK);
		
	}




	//--------------------------------------------------------
	//▼▼▼▼▼▼▼ 유영미 차자님 작성 ▼▼▼▼▼▼▼
	//--------------------------------------------------------

	//--------------------------------------------------------
	//login정보(시연:사번) 저장하기
	//--------------------------------------------------------
	@ApiOperation(value="Login(고객정보) 정보 입력하기")
	@RequestMapping(value="/login/{custNo}/{custNm}", method=RequestMethod.POST)
	public ResponseEntity <String > insertLoginInfo(
			@RequestBody LoginModel loginModel
			) throws Exception { 

		log.info("Start insert step info data");
		int re  = puzzleDao.insertLoginInfo(loginModel);
		log.debug("result :"+ re);

		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}




	//--------------------------------------------------------
	//login정보(고객정보) 가져온다.
	//--------------------------------------------------------
	@ApiOperation(value="Login(고객정보) 정보 가져오기")
	@RequestMapping(value="/login/{custNo}", method=RequestMethod.GET)
	public ResponseEntity <List<LoginModel>> getLoginInfo(
			@PathVariable (name="custNo", required = true) String custNo
			) {

		List<LoginModel> result = null;
		try {
			log.info("Start get Login Information");
			result = puzzleDao.selectLoginInfo(custNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("Login information :" + result.size());

		return new ResponseEntity<List<LoginModel>> (result, HttpStatus.OK);
	}	



	//--------------------------------------------------------
	//퍼즐 단계 정보를 조회한다.
	//--------------------------------------------------------
	@ApiOperation(value="퍼즐 단계 조회하기 ")
	@RequestMapping(value="/puzzle/stepsave/{custNo}/{prdCd}", method=RequestMethod.GET)
	public ResponseEntity <List<StepSaveModel>> getStepsaveInfo(
			@PathVariable (name="custNo", required = true) String custNo,@PathVariable (name="prdCd", required = true) String prdCd
			) {
	
		List<StepSaveModel> list = null;
		try {
			log.info("Start get StepSave  Infomation");
			list = puzzleDao.selectStepsaveInfo(custNo,prdCd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<StepSaveModel>> (list, HttpStatus.OK);
	}



	//--------------------------------------------------------
	//퍼즐 단계 정보를 등록한다.
	//--------------------------------------------------------
	@ApiOperation(value="퍼즐 단계 정보 등록하기")
	@RequestMapping(value="/puzzle/stepsave", method=RequestMethod.POST)
	public ResponseEntity <String > postStepSave(
			@RequestBody StepSaveModel stepSaveModel
			) throws Exception { 

		log.info("Start insert step info data");
		int re  = puzzleDao.insertStepSave(stepSaveModel);
		log.debug("result :"+ re);

		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}



	//--------------------------------------------------------
	//퍼즐 단계 정보를 갱신한다.
	//--------------------------------------------------------
	@ApiOperation(value="퍼즐 단계 정보 갱신하기")
	@RequestMapping(value="/puzzle/stepsave", method=RequestMethod.PUT)
	public ResponseEntity <String > putStepSave(
			@RequestBody StepSaveModel stepSaveModel
			) throws Exception { 

		log.info("Start update step info data");
		int re  = puzzleDao.updateStepSave(stepSaveModel);
		log.debug("result :"+ re);

		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}



	//--------------------------------------------------------
	//퍼즐 단계 정보를 삭제한다.
	//--------------------------------------------------------
	@ApiOperation(value="퍼즐 단계 정보 삭제하기")
	@RequestMapping(value="/puzzle/stepsave", method=RequestMethod.DELETE)
	public ResponseEntity <String > deleteStepSave(
			@RequestBody StepSaveModel stepSaveModel
			) throws Exception { 

		log.info("Start delete step info data");
		int re  = puzzleDao.deleteStepSave(stepSaveModel);
		log.debug("result :"+ re);

		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}


}