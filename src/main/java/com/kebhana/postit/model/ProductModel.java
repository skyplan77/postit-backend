package com.kebhana.postit.model;

import lombok.Data;

@Data
public class ProductModel {
	 private String  prdCd 			; // 상품코드
	 private String  prdName 		; // 상품명
	 private String  prdDesc		; // 상품내용 
	 private String  prdRate		; // 금리
	 private String  prdPeriod		; // 기관
	 private String  themeID		; // 테마ID
	 private String  termsId		; // 약관ID
	 private String  puzzlePath		; // 퍼즐경로
}