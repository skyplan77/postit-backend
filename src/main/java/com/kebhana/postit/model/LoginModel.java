package com.kebhana.postit.model;

import lombok.Data;

@Data
public class LoginModel {
    private String  custNo         ; // 고객번호
    private String  custNm         ; // 고객명
    private String  passWd         ; // password
}