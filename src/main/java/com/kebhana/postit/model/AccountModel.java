package com.kebhana.postit.model;

import lombok.Data;

@Data
public class AccountModel {
	 private String  acctNo 					; // 계좌번호
	 private String  custNo 					; // 고객번호
	 private String  prdType 					; // 상품종류
	 private String  prdCd 						; // 상품코드
	 private String  password 					; // 비밀번호
	 private String  prdName 					; // 상품명
	 private int  bal 							; // 잔액
	 private int  term 							; // 가입기간
	 private String  fundingMethod 				; // 적립방법
	 private String  transferYn 				; // 자동이체여부
	 private String  transferStart_date			; // 이체시작일
	 private String  transferAssignment_date 	; // 이체지정일
	 private String  transferTerm 				; // 이체간격
	 private int  transferAmt 					; // 이체금액
	 private int  newAmt 						; // 신규금액
	 private String  maturityGb 				; // 만기해지구분
	 private String  maturitySmsYn 				; // 만기sms통보신청여부
	 private String  outBank 					; // 출금은행
	 private String  outAcctno 					; // 출금계좌번호
}