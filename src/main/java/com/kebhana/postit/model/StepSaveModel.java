package com.kebhana.postit.model;

import lombok.Data;

@Data
public class StepSaveModel {
	private String custNo		; // 고객번호
	private String prdCd		; // 상품번호
	private String themeId		; //테마id
	private String puzzleId	; //퍼즐ID
	private String puzzlePath	; //퍼즐경로
	private String step			; //완료step
	private String saveDate		; //저장일시 - 5영업일만 보관목적 
	private String outBank;
	private String outAcctno;
	private String prdName;
	private int term;
	private String fundingMethod;
	private String transferYn;
	private String transferStart_date;
	private String transferAssignment_date;
	private String transferTerm;
	private int transferAmt;
	private int newAmt;
	private String maturityGb;
	private String maturitySmsYn; 
}