package com.kebhana.postit.model;

import lombok.Data;

@Data
public class PuzzleModel {
	 private String  puzzleId 		; // 퍼즐 코드
	 private String  puzzleName 	; // 퍼즐명
	 private String  puzzlePath		; // 퍼즐경로명
}
