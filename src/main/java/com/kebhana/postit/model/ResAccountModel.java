package com.kebhana.postit.model;

import java.util.List;

import lombok.Data;

@Data
public class ResAccountModel {
	private String resultMsg;		//응답 메시지
	private String resultCode;		// 응답코드
	private String accountNumber;	//계좌 번호
	private String winType;			//당첨종류
	private String winValue;		//당첨내용
//	private AccountModel accountModel;
//	private List<AccountModel> list;

}
