package com.kebhana.postit.model;

import lombok.Data;

@Data
public class TermsModel {
//	 private String  termsId 		; // 약관번호
	 private String  termsName 		; // 약관이름
	 private String  termsPath		; // 약관이미지경로
}