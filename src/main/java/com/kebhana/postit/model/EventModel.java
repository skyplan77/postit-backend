package com.kebhana.postit.model;

import lombok.Data;

@Data
public class EventModel {
	 private String  acctNo 					; // 계좌번호
	 private String  custNo 					; // 고객번호
	 private String  prdCd 						; // 상품코드
	 private String  prdName 					; // 상품명
	 private String  winType 					; // 당첨종류
	 private String  winValue 					; // 당첨내용
	 private String  winDate 					; // 당첨일
	 private String  themeId 					; // 테마id
	 private String  puzzleId 					; // 퍼즐id
//	 private String  sysdate 					; // sysdate
	 
}